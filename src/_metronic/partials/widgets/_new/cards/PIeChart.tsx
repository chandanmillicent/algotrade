import React from "react";
import DoughnutChart from "../../charts/DoughnutChart";
import { Row, Col } from "react-bootstrap";

const PIeChart = () => {
  const winRatio = 70; // Adjust the values based on your data
  const lossRatio = 30;
  const circumference = 180;
  const chartNumber = 18;
  const textBaseline = "top";
  //     const ChartWrapper = styled.div`
  //   height: 201px;
  //   position: relative;
  // `;

  // const DoughnutInner = styled.div`
  //   width: 100%;
  //   position: absolute;
  //   line-height: 20px;
  //   top: 50%;
  //   left: 0;
  //   margin-top: -22px;
  //   text-align: center;
  //   z-index: 0;
  // `;
  return (
    <div className="new-card">
      <div
        className="card-header"
        style={{ paddingBottom: "40px" }}
      >
        <div className="card-title d-flex flex-column">
          <span className="new-pie-chart-header">Limit Utilization</span>
        </div>
      </div>

      <div
        className="d-flex justify-content-center align-items-center"
        style={{ height: "270px", paddingBottom: "40px" }}
      >
        <DoughnutChart
          winRatio={winRatio}
          lossRatio={lossRatio}
          circumference={circumference}
          chartNumber={chartNumber}
          textBaseline={textBaseline}
        />
      </div>

      {/* <div className="d-flex position-absolute">
          <div className="d-flex justify-content-end align-items-center">
            <span>
              <div className="fs-13 light-font-text mb-20 ms-20 p-1 pb-12">
                Total units
              </div>

              <div className="table-property-name  fs-16"></div>
            </span>
          </div>
        </div> */}

      <div className="d-flex flex-column content-justify-center flex-row-fluid ">
        <div className="d-flex fw-semibold align-items-center">
          <div className=" flex-grow-1 me-4 card-sub-heading">
            Available limit
          </div>
          <div className="text-xxl-end card-sub-heading">Utilized limit</div>
        </div>

        <div className="d-flex fw-semibold align-items-center my-2">
          <div className=" flex-grow-1 me-4 card-number">198,73,980</div>
          <div className="text-xxl-end card-number">198,73,980</div>
        </div>
      </div>

      {/* <Row>
        <Col>
          <div className="card-sub-heading">Available limit</div>
          <div className=" flex-grow-1 me-4 card-number">198,73,980</div>
        </Col>

        <Col>
          <div className="card-sub-heading">Utilized limit</div>
          <div className=" flex-grow-1 me-4 card-number">198,73,980</div>
        </Col>
      </Row> */}

      {/* <div className="">
        <div className="d-xl-flex justify-content-xl-between align-items-xl-center">
          <div className="card-sub-heading">Available limit</div>
          <div className="card-number">198,73,980</div>
        </div>

        <div className="d-xl-flex justify-content-xl-between align-items-xl-center mt-2">
          <div className="card-sub-heading">Utilized limit</div>
          <div className="card-number">198,73,980</div>
        </div>
      </div> */}
    </div>
  );
};

export default PIeChart;
