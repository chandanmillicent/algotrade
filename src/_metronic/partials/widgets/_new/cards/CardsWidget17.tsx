/* eslint-disable @typescript-eslint/ban-ts-comment */
import { FC, useEffect, useRef } from "react";
import { KTIcon } from "../../../../helpers";
import { getCSSVariableValue } from "../../../../assets/ts/_utils";
import { useThemeMode } from "../../../layout/theme-mode/ThemeModeProvider";
import DoughnutChart from "../../charts/DoughnutChart";
import "../../../../../styles/chart.css";
import "../../../../../styles/common.css";
import { Row, Col } from "react-bootstrap";

type Props = {
  className?: string;
  chartSize?: number;
  chartLine?: number;
  chartRotate?: number;
};

const CardsWidget17: FC<Props> = ({
  className,
  chartSize = 70,
  chartLine = 11,
  chartRotate = 145,
}) => {
  const chartRef = useRef<HTMLDivElement | null>(null);
  const { mode } = useThemeMode();
  useEffect(() => {
    refreshChart();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [mode]);

  const refreshChart = () => {
    if (!chartRef.current) {
      return;
    }

    setTimeout(() => {
      initChart(chartSize, chartLine, chartRotate);
    }, 10);
  };
  const winRatio = 70; // Adjust the values based on your data
  const lossRatio = 30;
  const chartNumber = 1.5;
  const textBaseline = "middle";

  return (
    <div className="new-card">
      <div
        className="card-header"
        style={{ paddingBottom: "40px" }}
      >
        <div className="card-title d-flex flex-column">
          <span className="new-pie-chart-header">Win Loss Ratio</span>
        </div>
      </div>

      <div className="doughnut-chart-wrapper d-flex justify-content-center align-items-center">
        <DoughnutChart
          winRatio={winRatio}
          lossRatio={lossRatio}
          chartNumber={chartNumber}
          textBaseline={textBaseline}
        />
      </div>

      <div className="d-flex flex-column content-justify-center flex-row-fluid">
        <div className="d-flex fw-semibold align-items-center">
          <div className="flex-grow-1 me-4 card-sub-heading">Total Wins</div>
          <div className="text-xxl-end card-sub-heading">Total Loss</div>
        </div>

        <div className="d-flex fw-semibold align-items-center my-2">
          <div className="flex-grow-1 me-4 card-number">
            346.5 &nbsp;<span className="card-number-positive">12.56%</span>
          </div>
          <div className="text-xxl-end card-number">
            194.2 &nbsp;<span className="card-number-negative">12.56%</span>
          </div>
        </div>
      </div>

      {/* <div className="d-flex align-items-center justify-content-between">
        <div>
          <div className="card-sub-heading">Total Wins</div>
          <div className="card-number">
            346.5 <span className="card-number-positive">12.56%</span>
          </div>
        </div>

        <div>
          <div className="card-sub-heading">Total Loss</div>
          <div className="card-number">
            194.2 <span className="card-number-negative">12.56%</span>
          </div>
        </div>
      </div> */}

      {/* <Row>
        <Col>
          <div className="card-sub-heading">Total Wins</div>
          <div className="card-number">
            346.5 <span className="card-number-positive">12.56%</span>
          </div>
        </Col>

        <Col>
          <div>
            <div className="card-sub-heading">Total Loss</div>
            <div className="card-number">
              194.2 <span className="card-number-negative">12.56%</span>
            </div>
          </div>
        </Col>
      </Row> */}
    </div>
  );
};

const initChart = function (
  chartSize: number = 70,
  chartLine: number = 11,
  chartRotate: number = 145
) {
  const el = document.getElementById("kt_card_widget_17_chart");
  if (!el) {
    return;
  }
  el.innerHTML = "";

  const options = {
    size: chartSize,
    lineWidth: chartLine,
    rotate: chartRotate,
    //percent:  el.getAttribute('data-kt-percent') ,
  };

  const canvas = document.createElement("canvas");
  const span = document.createElement("span");

  //@ts-ignore
  if (typeof G_vmlCanvasManager !== "undefined") {
    //@ts-ignore
    G_vmlCanvasManager.initElement(canvas);
  }

  const ctx = canvas.getContext("2d");
  canvas.width = canvas.height = options.size;

  el.appendChild(span);
  el.appendChild(canvas);

  ctx?.translate(options.size / 2, options.size / 2); // change center
  ctx?.rotate((-1 / 2 + options.rotate / 180) * Math.PI); // rotate -90 deg

  //imd = ctx.getImageData(0, 0, 240, 240);
  const radius = (options.size - options.lineWidth) / 2;

  const drawCircle = function (
    color: string,
    lineWidth: number,
    percent: number
  ) {
    percent = Math.min(Math.max(0, percent || 1), 1);
    if (!ctx) {
      return;
    }

    ctx.beginPath();
    ctx.arc(0, 0, radius, 0, Math.PI * 2 * percent, false);
    ctx.strokeStyle = color;
    ctx.lineCap = "round"; // butt, round or square
    ctx.lineWidth = lineWidth;
    ctx.stroke();
  };

  // Init 2
  drawCircle("#E4E6EF", options.lineWidth, 100 / 100);
  drawCircle(getCSSVariableValue("--bs-primary"), options.lineWidth, 100 / 150);
  drawCircle(getCSSVariableValue("--bs-success"), options.lineWidth, 100 / 250);
};

export { CardsWidget17 };
