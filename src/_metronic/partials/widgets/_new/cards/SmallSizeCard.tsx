import React, { useEffect, useState } from "react";
import { Col, Row } from "react-bootstrap";
import { useMyContext } from "../../../../../app/modules/auth/core/MyContext";
import { toAbsoluteUrl } from "../../../../helpers";
import { getStrategyCardData } from "../../../../../app/modules/auth/core/_requests";
import { IStrategyData } from "../../../../../app/modules/auth/core/_models";

interface User {
  id: number;
  username: string;
  full_name: string;
  email: string;
  age: number;
  city: string;
}
interface ISmallSizeCard {
  context: string;
  headerType: string;
}

const SmallSizeCard: React.FC<ISmallSizeCard> = ({ context, headerType }) => {
  const [strategydata, setStrategyData] = useState<IStrategyData[] | null>(
    null
  );
  console.log("🚀 ~ strategydata:", strategydata);
  useEffect(() => {
    const fetchData = async (context: string, headerType: string) => {
      const response = await getStrategyCardData(context, headerType);
      setStrategyData(response);
      console.log("response", response);
    };
    fetchData(context, headerType);
    // console.log(strategydata, "strategydata")
  }, [context, headerType]);

  // console.log(strategydata, "strategydata", context)
  const { secondStep, setSecondStep } = useMyContext();
  return (
    <div>
      <Row>
        {strategydata?.map((data: any) => (
          <Col sm={3}>
            <div
              key={data.dataId}
              onClick={() => setSecondStep(true)}
              className="new-small-size-card d-flex align-items-center justify-content-between"
              style={{ cursor: "pointer" }}
            >
              <div>
                <div className="card-sub-heading">{data.name}</div>
                <div className="card-number mt-3">{data.value}</div>
              </div>

              <img
                src={toAbsoluteUrl("media/pie-chart/piechart.png")}
                className="h-20px"
                alt=""
              />
              {/* <div className="d-lg-flex justify-content-between">
                <p>{data.username}</p>
                <div className="d-flex justify-content-end">
                  <img
                    src={toAbsoluteUrl("media/pie-chart/piechart.png")}
                    className="h-20px"
                    alt=""
                  />
                </div>
              </div> */}
            </div>
          </Col>
        ))}
      </Row>
    </div>
  );
};

export default SmallSizeCard;
