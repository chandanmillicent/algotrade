// import ReactApexChart from "react-apexcharts";

// interface DoughnutChartProps {
//   winRatio: number;
//   lossRatio: number;
//   width?: string | number; // Allow external width customization
// }

// const DoughnutChart: React.FC<DoughnutChartProps> = ({ winRatio, lossRatio, width = '400',circumference }) => {
//   const totalRatio = winRatio + lossRatio;

//   const options = {
//     labels: ['Win', 'Loss'],
//     colors: ['#4CAF50', '#FF5252'],
//     legend: {
//       show: false,
//     },
//     dataLabels: {
//       enabled: false, // Disable the default percentage labels
//     },
//     plotOptions: {
//       pie: {
//         startAngle: -90,
//         endAngle: 90,
//         offsetY: 10
//       }
//     },
//     annotations: {
//       points: [
//         {
//           x: 0,
//           y: 0,
//           yAxisIndex: 0,
//           label: {
//             text: `<div class="fs-13 light-font-text">Total units</div><div class="fs-16">${totalRatio}%</div>`,
//             borderColor: '#777',
//             style: {
//               fontSize: '12px',
//               color: '#fff',
//               background: '#777',
//             },
//           },
//         },
//       ],
//     },
//   };

//   const series = [winRatio, lossRatio];

//   return (
//     <ReactApexChart
//       options={options}
//       series={series}
//       type="donut"
//       width={width}
//     />
//   );
// };

// export default DoughnutChart;

import { elements } from "chart.js";
import React, { useEffect } from "react";
import { Doughnut } from "react-chartjs-2";

interface DoughnutChartProps {
  winRatio: number;
  lossRatio: number;
  circumference?: number;
  showTextCenter?: boolean;
  chartNumber: number;
  textBaseline: string;
}

const DoughnutChart: React.FC<DoughnutChartProps> = ({
  winRatio,
  lossRatio,
  circumference,
  chartNumber,
  textBaseline,
}) => {
  const data = {
    labels: ["Win", "Loss"],
    datasets: [
      {
        data: [winRatio, lossRatio],
        backgroundColor: ["#4CAF50", "#FF5252"],
      },
    ],
  };

  const options = {
    maintainAspectRatio: true,
    cutout: "65%",
    // legend: {
    //   display: false,
    //   // position: "left",
    // },
    plugins: {
      legend: {
        display: false,
      },
    },
    responsive: true,
    circumference: circumference ? circumference : 360,
    rotation: -90,
  };

  const plugins = [
    {
      id: "textCenter",
      beforeDraw: function (chart: any) {
        const width = chart.width,
          height = chart.height,
          ctx = chart.ctx;
        // console.log("ctx", ctx);
        ctx.restore();
        const fontSize = (height / 160).toFixed(2);
        ctx.fillStyle = "#41414e";
        ctx.font = "normal 30px inter";
        ctx.textBaseline = textBaseline;
        const text = chartNumber,
          textX = Math.round((width - ctx.measureText(text).width) / 2),
          textY = height / 2;
        ctx.fillText(text, textX, textY);
        ctx.save();
      },
    },
  ];

  // useEffect(() => {
  //   const chart = document.getElementById('doughnut-chart') as HTMLCanvasElement;
  //   const context = chart?.getContext('2d');

  //   if (context) {
  //     context.font = '20px Arial'; // Adjust font size as needed
  //     context.textAlign = 'center';
  //     context.fillText(`Total units: ${totalRatio}%`, chart.width / 2, chart.height / 2);
  //   }
  // }, [totalRatio]);

  return (
    <Doughnut
      id="doughnut-chart"
      data={data}
      options={options}
      // plugins={[textCenter]}
      plugins={plugins}
    />
  );
};

export default DoughnutChart;
