import { useEffect, useRef, FC, useState } from "react";
import ApexCharts, { ApexOptions } from "apexcharts";
import { getCSS, getCSSVariableValue } from "../../../assets/ts/_utils";
import { useThemeMode } from "../../layout/theme-mode/ThemeModeProvider";
import "../../../../../src/styles/chart.css";
import "../../../../../src/styles/common.css";

type Props = {
  className: string;
};

const ChartsWidget6: FC<Props> = ({ className }) => {
  const chartRef = useRef<HTMLDivElement | null>(null);
  const [buttonColor, setButtonColor] = useState<"Year" | "Month" | "Week">(
    "Year"
  );
  const { mode } = useThemeMode();
  const refreshChart = () => {
    if (!chartRef.current) {
      return;
    }

    const height = parseInt(getCSS(chartRef.current, "height"));

    const chart = new ApexCharts(chartRef.current, getChartOptions(height));
    if (chart) {
      chart.render();
    }

    return chart;
  };

  useEffect(() => {
    const chart = refreshChart();

    return () => {
      if (chart) {
        chart.destroy();
      }
    };
  }, [chartRef, mode]);

  return (
    <div className="new-card">
      {/* begin::Header */}
      <div className="d-flex border-0 flex-column flex-sm-row justify-content-sm-between">
        <div className="card-title d-flex flex-column">
          <span className="new-pie-chart-header">Performance</span>
        </div>

        <div className="new-card-toolbar mt-3 mt-sm-0 flex-row justify-content-between">
          {/* <ul className="nav"> */}
          {/* <li> */}
          <button
            className={`${
              buttonColor === "Year" ? "bg-white " : "bg-transparent"
            } fw-bold me-1 px-2 py-1 rounded border-0`}
            data-bs-toggle="tab"
            onClick={() => setButtonColor("Year")}
          >
            Year
          </button>
          {/* </li> */}
          {/* <li className="nav-item"> */}
          <button
            className={`${
              buttonColor === "Month" ? "bg-white " : "bg-transparent"
            } fw-bold me-1 px-2 py-1 rounded border-0`}
            data-bs-toggle="tab"
            onClick={() => setButtonColor("Month")}
          >
            Month
          </button>
          {/* </li> */}
          {/* <li className="nav-item"> */}
          <button
            className={`${
              buttonColor === "Week" ? "bg-white " : "bg-transparent"
            } fw-bold me-1 px-2 py-1 rounded border-0`}
            data-bs-toggle="tab"
            onClick={() => setButtonColor("Week")}
          >
            Week
          </button>
          {/* </li> */}
          {/* </ul> */}
        </div>

        {/* end::Toolbar */}
      </div>

      <div className="level card-header">
        <div className="level is-mobile d-flex gap-17">
          <div>
            <p className="card-sub-heading">Average</p>
            <p className="card-number">
              29,949 &nbsp;<span className="card-number-positive"> 12.56%</span>
            </p>
          </div>

          <div>
            <p className="card-sub-heading">This week</p>
            <p className="card-number">
              29,949 &nbsp;<span className="card-number-positive"> 12.56%</span>
            </p>
          </div>
        </div>
      </div>

      {/* begin::Body */}
      <div
        // className="card-body"
        style={{ height: "250px" }}
      >
        {/* begin::Chart */}
        <div
          ref={chartRef}
          id="kt_charts_widget_6_chart"
          style={{ height: "250px" }}
        ></div>
        {/* end::Chart */}
      </div>
      {/* end::Body */}
    </div>
  );
};

export { ChartsWidget6 };

function getChartOptions(height: number): ApexOptions {
  const labelColor = getCSSVariableValue("--bs-gray-500");
  const borderColor = getCSSVariableValue("--bs-gray-200");

  const baseColor = getCSSVariableValue("--bs-teal");
  const baseLightColor = getCSSVariableValue("--bs-primary-light");
  const secondaryColor = getCSSVariableValue("--bs-info");
  const baseColor1 = " #00B386";
  interface ChartData {
    month: string;
    amount: number;
    color: string;
    type:string;
  }
 

  const data: ChartData[] = [
    {
      amount: 12345,
      month: "january",
      type: "profit",
      color: "#00FF00",
    },
    {
      amount: 1245,
      month: "july",
      type: "profit",
      color: "#00FF00",
    },
    {
      amount: 1235,
      month: "january",
      type: "profit",
      color: "#00FF00",
    },
    {
      amount: -12345,
      month: "march",
      type: "loss",
      color: "#FF0000",
    },
  ];

    // Group the data by month
  //   const groupedData: Record<string, { amount: number; color: string }[]> = data.reduce((acc, item) => {
  //     if (!acc[item.month]) {
  //       acc[item.month] = [];
  //     }
  //     acc[item.month].push({
  //       amount: item.amount,
  //       color: item.color,
  //     });
  //     return acc;
  //   }, {});
  
  //   // Prepare data for ApexCharts
  //   const chartData = Object.keys(groupedData).map(month => ({
  //     name: month,
  //     data: groupedData[month].map(item => item.amount),
  //     color: groupedData[month][0].color,
  //   }));


  // const barData=data?.map((data)=>data?.color)

  // const dummyBarData: dummyBarData[] = [
  //   {
  //     id: 1,
  //     month: "jan",
  //     key: "loss",
  //     amount: "40000",
  //     color:"red",

  //   },
  //   {
  //     id: 2,
  //     month: "feb",
  //     key: "profit",
  //     amount: "400",
  //     color:"green",

  //   },
  //   {
  //     id: 3,
  //     month: "march",
  //     key: "loss",
  //     amount: "70000",
  //     color:"red",

  //   },
  //   {
  //     id: 4,
  //     month: "april",
  //     key: "loss",
  //     amount: "20000",
  //     color:"red",
  //   },

  //   // Add more users as needed
  // ];
  // const color = dummyBarData?.map((dummyBarData: any) => dummyBarData?.color);
  // console.log("🚀 ~ color:", color);

  return {
    series: [
      {
        name: "Net Profit",
        type: "bar",
        data: data?.map((data)=>data?.amount),
      },
     
    ],
    
    chart: {
      fontFamily: "inherit",
      stacked: true,
      height: "200",
      toolbar: {
        show: false,
      },
    },
    plotOptions: {
      bar: {
        colors: {
          ranges: [{
            from: -Infinity,
            to: 0,
            color: '#FF0000', 
          }, {
            from: 1,
            to: 100000,
            color: '#00FF00', 
          }],
        },
        horizontal: false,
            borderRadius: 5,
            columnWidth: "39%",
      },
    },
   
    legend: {
      show: false,
    },
    dataLabels: {
      enabled: false,
    },
    stroke: {
      curve: "smooth",
      show: true,
      width: 2,
      colors: ["transparent"],
    },
    // {dummyBarData.map((dummyBarData: any) => (
    xaxis: {
      categories:data?.map((data)=>data?.month),
      axisBorder: {
        show: false,
      },
      axisTicks: {
        show: false,
      },
      labels: {
        style: {
          colors: labelColor,
          fontSize: "12px",
        },
      },
    },
    yaxis: {
      max: 120,
      min: 0,
      tickAmount: 4,
      labels: {
        style: {
          colors: labelColor,
          fontSize: "12px",
        },
      },
    },
    fill: {
      opacity: 1,
    },
    states: {
      normal: {
        filter: {
          type: "none",
          value: 0,
        },
      },
      hover: {
        filter: {
          type: "none",
          value: 0,
        },
      },
      active: {
        allowMultipleDataPointsSelection: false,
        filter: {
          type: "none",
          value: 0,
        },
      },
    },
    tooltip: {
      style: {
        fontSize: "12px",
      },
      // y: {
      //   formatter: function (val) {
      //     return "$" + val + " thousands";
      //   },
      // },
    },
    grid: {
    
      strokeDashArray: 4,
      yaxis: {
        lines: {
          show: true,
        },
      },
      padding: {
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
      },
    },
  };
}
