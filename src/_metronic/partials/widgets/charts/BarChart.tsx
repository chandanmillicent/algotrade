import React, { useEffect, useState } from "react";
import Chart from "react-apexcharts";
import { Col, Row } from "react-bootstrap";
import { IStretagyBarChart } from "../../../../app/modules/auth";
import { getStrategyBarChart } from "../../../../app/modules/auth/core/_requests";

interface IBarChart {
  buyselltype: string;
  headerType: string;
}
const BarChart: React.FC<IBarChart> = ({ buyselltype, headerType }) => {
  const [selectedTab, setSelectedTab] = useState<string>("Year");
  const [strategyData, setStrategyData] = useState<IStretagyBarChart[]>([]);
  // console.log(selectedTab, "selectedTab")

  useEffect(() => {
    const fetchData = async () => {
      const response = await getStrategyBarChart(
        headerType,
        buyselltype,
        selectedTab
      );
      setStrategyData(response);
    };
    fetchData();
    // console.log(strategydata, "strategydata")
  }, [buyselltype, headerType, selectedTab]);

  // const data = [
  //   {
  //     amount: 12345,
  //     month: "Jan",
  //     type: "profit",
  //     color: "#00FF00",
  //   },
  //   {
  //     amount: 16045,
  //     month: "Jul",
  //     type: "profit",
  //     color: "#00FF00",
  //   },
  //   {
  //     amount: -8000,
  //     month: "january",
  //     type: "loss",
  //     color: "#00FF00",
  //   },
  //   {
  //     amount: -4000,
  //     month: "Mar",
  //     type: "loss",
  //     color: "#FF0000",
  //   },
  //   {
  //     amount: -19000,
  //     month: "Mar",
  //     type: "loss",
  //     color: "#FF0000",
  //   },

  //   // Add more months as needed
  // ]
  const data = strategyData;
  console.log(data, "data2");
  const options = {
    chart: {
      type: "bar" as const,
      toolbar: {
        show: false,
      },
      offsetX: -20,
    },
    plotOptions: {
      bar: {
        colors: {
          ranges: [
            {
              from: -Infinity,
              to: 0,
              color: "#FF5252", // color for loss (negative profit)
              // color: data?.map(color => color?.color),
            },
            {
              from: 1,
              to: Infinity, // adjust based on your profit range
              color: "#4CAF50", // color for profitable months
            },
          ],
        },
        columnWidth: "35%",
      },
    },
    legend: {
      show: false,
    },
    dataLabels: {
      enabled: false,
    },
    stroke: {
      curve: "smooth" as const,
      show: true,
      width: 2,
      colors: ["transparent"],
    },
    xaxis: {
      categories: data.map((month) => {
        let val = month.month;
        val = val.split(" ")[0].slice(0, 3) + "'" + val.split(" ")[1].slice(-2);
        return val;
      }),
      labels: {
        style: {
          // colors: labelColor,
          fontSize: "12px !important",
          color: "#969ba1 !important",
          fontWeight: "normal !important",
        },
      },
      axisBorder: {
        show: false,
      },
      axisTicks: {
        show: false,
      },
      yaxis: {
        max: 120,
        min: 0,
        tickAmount: 4,
        labels: {
          style: {
            // colors: labelColor,
            fontSize: "12px !important",
            color: "#969ba1 !important",
            fontWeight: "normal !important",
          },
        },
      },
    },

    fill: {
      opacity: 1,
    },
    states: {
      normal: {
        filter: {
          type: "none",
          value: 0,
        },
      },
      hover: {
        color: null, // Set to null to remove the shadow effect
        filter: {
          type: "none",
          value: 0,
        },
      },
      active: {
        allowMultipleDataPointsSelection: false,
        filter: {
          type: "none",
          value: 0,
        },
      },
    },
    tooltip: {
      style: {
        fontSize: "12px",
      },
      // y: {
      //   formatter: function (val) {
      //     return "$" + val + " thousands";
      //   },
      // },
    },
    grid: {
      // borderColor: borderColor,
      strokeDashArray: 4,
      yaxis: {
        lines: {
          show: true,
        },
      },
      padding: {
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
      },
    },
  };

  const series = [
    {
      name: "Profit",
      data: data?.map((month) => month.amount),
    },
  ];

  return (
    <div className="new-card">
      {/* header */}
      <div className="d-flex border-0 flex-column flex-sm-row justify-content-sm-between">
        <div className="card-title d-flex flex-column">
          <span className="new-pie-chart-header">Performance</span>
        </div>

        <div className="new-card-toolbar mt-5 mt-sm-0 flex-row justify-content-between">
          <button
            className={`${
              selectedTab === "Year" ? "bg-white " : "bg-transparent"
            } fw-bold me-1 px-2 py-1 rounded border-0 w-100`}
            data-bs-toggle="tab"
            onClick={() => setSelectedTab("Year")}
            style={{
              color: selectedTab == "Year" ? "#41414e" : "#969ba1",
            }}
          >
            Year
          </button>

          <button
            className={`${
              selectedTab === "Month" ? "bg-white " : "bg-transparent"
            } fw-bold me-1 px-2 py-1 rounded border-0 w-100`}
            data-bs-toggle="tab"
            onClick={() => setSelectedTab("Month")}
            style={{
              color: selectedTab == "Month" ? "#41414e" : "#969ba1",
            }}
          >
            Month
          </button>

          <button
            className={`${
              selectedTab === "Week" ? "bg-white " : "bg-transparent"
            } fw-bold me-1 px-2 py-1 rounded border-0 w-100`}
            data-bs-toggle="tab"
            onClick={() => setSelectedTab("Week")}
            style={{
              color: selectedTab == "Week" ? "#41414e" : "#969ba1",
            }}
          >
            Week
          </button>
        </div>
      </div>

      {/* <div className="level d-flex justify-content-between">
        <div className="level is-mobile d-flex gap-17 mt-5 mt-md-0">
          <div>
            <div className="card-sub-heading">Average</div>
            <div className="card-number fs-12 mt-1">
              29,949 &nbsp;<span className="card-number-positive"> 12.56%</span>
            </div>
          </div>

          <div>
            <div className="card-sub-heading">This week</div>
            <div className="card-number mt-1">
              29,949 &nbsp;<span className="card-number-positive"> 12.56%</span>
            </div>
          </div>
        </div>

        <div className="d-flex align-items-end">
          <div
            className="w-2 h-2 rounded-4 p-3 me-2"
            style={{ backgroundColor: "#4CAF50" }}
          ></div>
          <div className="table-data">This {selectedTab}</div>
        </div>
      </div> */}

      <Row className="mt-5">
        <Col
          xs={6}
          sm={3}
        >
          <div>
            <div className="card-sub-heading">Average</div>
            <div className="card-number fs-12 mt-1">
              29,949 &nbsp;<span className="card-number-positive"> 12.56%</span>
            </div>
          </div>
        </Col>

        <Col
          xs={6}
          sm={3}
        >
          <div>
            <div className="card-sub-heading">This week</div>
            <div className="card-number mt-1">
              29,949 &nbsp;<span className="card-number-positive"> 12.56%</span>
            </div>
          </div>
        </Col>

        <Col
          className="d-flex justify-content-sm-end justify-content-start mt-5 mt-sm-0 align-items-end"
          // xs={4}
          sm={{ offset: 3, span: 3 }}
        >
          <div className="d-flex align-items-center">
            <div
              className="rounded-4 p-2 me-2"
              style={{
                backgroundColor: "#4CAF50",
                width: "16px",
                height: "16px",
              }}
            ></div>
            <div className="table-data text-align-right">
              This {selectedTab}
            </div>
          </div>
        </Col>
      </Row>

      {/* chart */}
      <div className="bar-chart-container">
        <Chart
          options={options}
          series={series}
          type="bar"
          height={"100%"}
          width={"102%"}
        />
      </div>
    </div>
  );
};

export default BarChart;
