const RadioJsonData = {
    RadioJsonData: [{
            label:"All",
            value:"All"
        },{
            label:"Only Buy",
            value:"Buy"
        },{
            label:"Only Sell",
            value:"Sell"
        },],
    radioButtonForPositions: [{
            label:"Open",
            value:"Open"
        },{
            label:"Close",
            value:"Close"
        },{
            label:"Hedge",
            value:"Hedge"
        },]
  };
  
  export default RadioJsonData;
  
  
  