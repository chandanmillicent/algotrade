import React, { useEffect, useState } from "react"
import RadioJsonData from "./RadioJsonData"
interface IButtonAction {
  setContext: (value: string) => void
  selectedTab:string
}

const RadioButton: React.FC<IButtonAction> = ({ setContext,selectedTab }) => {
  const [radioBtn, setRadioBtn] = useState<string>("")
  const handleClick = (e: any) => {
    setContext(e.target.value)
  }

  const checkSelectedTab =
    selectedTab === "Analysis"
      ? RadioJsonData.RadioJsonData
      : RadioJsonData.radioButtonForPositions

  useEffect(() => {
    // Set the default value for the first radio button when the component mounts
    setRadioBtn(checkSelectedTab[0]?.value || "")
  }, [checkSelectedTab])
  return (
    <>
      <div className="gap-4 d-flex ">
        {checkSelectedTab?.map((data, index) => (
          <div key={data.value} className="align-items-center d-flex">
            <input
              type="radio"
              name="inlineRadioOptions"
              id={`inlineRadio${data.value}`}
              value={data.value}
              onChange={e => setRadioBtn(e.target.value)}
              className="new-accent cursor-pointer"
              onClick={() => handleClick(data.value)}
              defaultChecked={index == 0} // Check the first radio button by default
            />
            <label
              className={
                radioBtn === data?.value
                  ? "radio-btn-label-checked ms-2"
                  : "ms-2 radio-btn-label"
              }
              htmlFor={`inlineRadio${data.value}`}
            >
              {data.label}
            </label>
          </div>
        ))}
      </div>
    </>
  )
}

export default RadioButton
