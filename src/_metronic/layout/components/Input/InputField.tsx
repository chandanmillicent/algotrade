import React from "react";
import "../../../../styles/common.css";

interface IInputFieldProps {
  type: string;
  placeholder: string;
  id: string;
  maxLength: number;
  value: string;
  onChange: () => void;
  iserror: boolean;
  disabled: boolean;
  defaultValue: string;
  removeOpacity: boolean;
  showValueCount: boolean;
  min: number;
  textTransform: string;
  handleBlur: void;
  fieldType: string;
  fieldName: string;
  subText: string;
}

const InputField: React.FC<IInputFieldProps> = ({
  type,
  placeholder,
  id,
  maxLength = 30,
  value,
  onChange,
  iserror,
  // errormessage,
  // errorscope = true,
  disabled = false,
  defaultValue = "",
  removeOpacity = false,
  showValueCount = false,
  min,
  textTransform = "",
  handleBlur = () => {},
  fieldType = "",
  fieldName = "",
  subText = "",
}) => {
  const valueCount = value?.length ? value?.length : 0;
  return (
    <>
      <div className="position-relative ">
        <div
          className={` ${iserror ? "form-error-input " : "form-input"}`}
          style={{ opacity: disabled && !removeOpacity ? "0.7" : "1" }}
        >
          <input
            type={type}
            placeholder={placeholder}
            id={id}
            // style={{ textTransform: textTransform ? textTransform : "" }}
            maxLength={maxLength}
            value={value}
            onChange={onChange}
            disabled={disabled}
            defaultValue={defaultValue}
            min={min}
            // onBlur={() => handleBlur(fieldType, fieldName, value)}
          />
          <label
            htmlFor={id}
            style={{ fontWeight: "300", color: " #969ba1" }}
          >
            {placeholder}
          </label>
        </div>
      </div>
    </>
  );
};

export default InputField;
