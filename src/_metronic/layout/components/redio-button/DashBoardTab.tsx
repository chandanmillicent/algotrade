import { Nav } from "react-bootstrap";
import { useMyContext } from "../../../../app/modules/auth/core/MyContext";
import { IStretagyHeader } from "../../../../app/modules/auth";

interface Itabs {
  headerData: IStretagyHeader[] | null;
}
interface tabsForSecondStep {
  id: number;
  display_name: string;
}

const DashBoardTab: React.FC<Itabs> = ({ headerData }: Itabs) => {
  const tabs = headerData ? headerData : [{ id: 1, display_name: "All" }];
  const tabsForSecondStep: tabsForSecondStep[] = [
    {
      id: 1,
      display_name: "Overview",
    },
    {
      id: 2,
      display_name: "Performance Summary",
    },
    {
      id: 3,
      display_name: "List of Trades",
    },
    // Add more users as needed
  ];
  const { secondStep } = useMyContext();

  const isSecond = secondStep ? tabsForSecondStep : tabs;
  return (
    <div className="level card-header">
      <div className="level is-mobile d-flex gap-17">
        <div>
          <p className="heading d-flex gap-3 ">
            <Nav
              variant="pills"
              defaultActiveKey={tabs ? "0" : "1"}
            >
              <Nav.Item className="d-flex gap-3 flex-wrap">
                {isSecond.map((tab) => (
                  <Nav.Link
                    className="nav-tab"
                    eventKey={tab.id}
                  >
                    {tab.display_name}
                  </Nav.Link>
                ))}
              </Nav.Item>
            </Nav>
          </p>
        </div>
      </div>
    </div>
  );
};
export default DashBoardTab;
