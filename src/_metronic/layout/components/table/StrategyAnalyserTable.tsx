import { DiffieHellman } from "crypto";
import React, { useEffect, useState } from "react";
import DateRangeFilter from "../dateRangePicker/DateRangeFilter";
import Dropdown from "react-bootstrap/Dropdown";
import DropdownButton from "react-bootstrap/DropdownButton";
import { getStrategyOverview } from "../../../../app/modules/auth/core/_requests";
import { ITableData } from "../../../../app/modules/auth";
// import { set } from "date-fns"
// import { Pagination } from "react-bootstrap"
// import { UsersListPagination } from "../../../../app/modules/apps/user-management/users-list/components/pagination/UsersListPagination"
import TableWithPagination from "../tableWithPagination/TableWithPagination";
import { useMyContext } from "../../../../app/modules/auth/core/MyContext";

interface ITableModel {
  context: string;
  headerType: string;
}
// React.FC<ISmallSizeCard> = ({ context }) => {
const StrategyAnalyserTable: React.FC<ITableModel> = ({
  context,
  headerType,
}) => {
  const [selectedTab, setSelectedTab] = useState<string>("Overview");
  const [tableData, setTableData] = useState<ITableData[]>([]);
  const [sortColumn, setSortColumn] = useState("");
  const [paginatedData, setPaginatedData] = useState<ITableData[]>(tableData);
  const [sortedData, setSortedData] = useState(paginatedData);
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [totalPages, setTotalPages] = useState<number>(1);
  const [sortingId, setSortingId] = useState<boolean>();
  const [selectedDate, setSelectedDate] = useState<object>({});
  const [dropDown, setDropDown] = useState<boolean>(false);
  const [visibleData, setVisibleData] = useState<number | undefined>(undefined);

  const { secondStep } = useMyContext();
  useEffect(() => {
    setSortedData(tableData);
  }, [tableData]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await getStrategyOverview(context, headerType);
        console.log("Log: tabel ", response);
        setTableData(response);
      } catch (err) {
        console.log(err);
      }
    };
    fetchData();
  }, [context]);

  useEffect(() => {
    console.log("pd", paginatedData);
  }, [paginatedData]);
  // const data = [
  //   {
  //     id: 1,
  //     display_name: "display1 name",
  //     strategy_name: "strategy name1",
  //     time_frame: "5 min",
  //     buy_sell: "buy",
  //     exit_type: "exit",
  //     array_data: {
  //       type: {
  //         type1: "exit1 logs",
  //         type2: "exit2 logs",
  //       },
  //       entry_signal_time: {
  //         type1: "12:49:59",
  //         type2: "2025-2-03",
  //       },
  //       target_stop_loss: {
  //         type1: "4421",
  //         type2: "1655",
  //       },
  //       signal: {
  //         type1: "exit1 logs",
  //         type2: "Buy",
  //       },
  //       time_frame: {
  //         time_frame1: " 10:12",
  //         time_frame2: "12:04",
  //       },
  //       date_time: {
  //         date_time1: "2023-11-21 10:12",
  //         date_time2: "2023-12-21 12:04",
  //       },
  //       price: {
  //         price1: "323423 INR",
  //         price2: "235346 INR",
  //       },
  //       entry_time_price: {
  //         type1: "231",
  //         type2: "464",
  //       },
  //       exit_time_price: {
  //         type1: "123456",
  //         type2: "0000",
  //       },
  //       pl_absolute: {
  //         type1: "654",
  //         type2: "321564",
  //       },
  //       pl_percent: {
  //         type1: "20%",
  //         type2: "30%",
  //       },
  //     },
  //     contract: 1,
  //     cum_profit: "-59.25 INR -0.28%",
  //     cum_profit: "470.25 INR -0.1%",
  //   },
  //   {
  //     id: 2,
  //     display_name: "display2 name",
  //     strategy_name: "strategy name2",
  //     time_frame: "10 min",
  //     buy_sell: "buy",
  //     exit_type: "exit",
  //     array_data: {
  //       type: {
  //         type1: "exit1 logs",
  //         type2: "exit2 logs",
  //       },
  //       entry_signal_time: {
  //         type1: "12:49:59",
  //         type2: "2022-1-18",
  //       },
  //       target_stop_loss: {
  //         type1: "4421",
  //         type2: "1655",
  //       },
  //       signal: {
  //         type1: "exit1 logs",
  //         type2: "Buy",
  //       },
  //       date_time: {
  //         date_time1: "2023-11-21 10:12",
  //         date_time2: "2023-12-21 12:04",
  //       },
  //       price: {
  //         price1: "323423 INR",
  //         price2: "235346 INR",
  //       },
  //       entry_time_price: {
  //         type1: "231",
  //         type2: "464",
  //       },
  //       exit_time_price: {
  //         type1: "123456",
  //         type2: "0000",
  //       },
  //       pl_absolute: {
  //         type1: "654",
  //         type2: "321564",
  //       },
  //       pl_percent: {
  //         type1: "20%",
  //         type2: "30%",
  //       },
  //     },
  //     contract: 1,
  //     cum_profit: "-59.25 INR -0.28%",
  //     cum_profit: "470.25 INR -0.1%",
  //   },
  // ]

  // console.log("tableData " + tableData?.splice(1, 2));

  const data = tableData;
  const handleSort = (columnName: string) => {
    // Toggle between ascending and descending order
    const newSortOrder = sortColumn === columnName ? !sortingId : true;
    setSortingId(newSortOrder);

    // setSortingId(columnName);
    let sorted;

    if (sortColumn === columnName) {
      // If already sorted by the same column, reverse the order
      sorted = [...sortedData]?.reverse();
    } else {
      // Sort the data based on the selected column and sorting order
      switch (columnName) {
        // case "date_time":
        //   sorted = [...sortedData]?.sort(
        //     (a, b) =>
        //       (new Date(a.array_data.date_time.date_time1) -
        //         new Date(b.array_data.date_time.date_time1)) *
        //       (newSortOrder ? 1 : -1)
        //   );
        //   break;
        // case "price":
        //   sorted = [...sortedData]?.sort(
        //     (a, b) =>
        //       (parseInt(a.array_data.price.price1, 10) -
        //         parseInt(b.array_data.price.price1, 10)) *
        //       (newSortOrder ? 1 : -1)
        //   );
        //   break;
        case "cum_profit":
          sorted = [...sortedData].sort(
            (a, b) =>
              (parseFloat(a.cum_profit) - parseFloat(b.cum_profit)) *
              (newSortOrder ? 1 : -1)
          );
          break;
        default:
          sorted = [...sortedData];
      }
    }

    setSortedData(sorted);
    setSortColumn(columnName);
  };

  // const handleSort = (columnName: string) => {
  //   console.log("🚀 ~ handleSort ~ columnName:", columnName)
  //   setSortingId(columnName)
  //   let sorted;

  //   if (sortColumn === columnName) {
  //     // If already sorted by the same column, reverse the order
  //     sorted = [...sortedData].reverse();
  //   } else {
  //     // Sort the data based on the selected column
  //     switch (columnName) {
  //       case "date_time":
  //         sorted = [...sortedData].sort(
  //           (a, b) =>
  //             new Date(a.array_data.date_time.date_time1) -
  //             new Date(b.array_data.date_time.date_time1)
  //         );
  //         break;
  //       case "price":
  //         sorted = [...sortedData].sort(
  //           (a, b) =>
  //             parseInt(a.array_data.price.price1, 10) -
  //             parseInt(b.array_data.price.price1, 10)
  //         );
  //         break;
  //       case "cum_profit":
  //         sorted = [...sortedData].sort(
  //           (a, b) => parseFloat(a.cum_profit) - parseFloat(b.cum_profit)
  //         );
  //         break;
  //       default:
  //         sorted = [...sortedData];
  //     }
  //   }

  //   setSortedData(sorted);
  //   setSortColumn(columnName);
  // };

  // const onChange = (ranges: any) => {
  //   console.log("reanges", ranges);
  // };

  return (
    <div className="new-table-card">
      <div>
        <div className="child-strategy-analyzer-table">
          <div className="d-flex flex-column flex-md-row justify-content-between align-items-start align-items-md-center mb-5">
            <div className="new-card-toolbar mb-5 mb-md-0 flex-wrap flex-md-nowrap">
              <button
                className={`${
                  selectedTab === "Overview" ? "bg-white " : "bg-transparent "
                } fw-bold me-1 px-2 py-1 rounded border-0 `}
                data-bs-toggle="tab"
                onClick={(e) => setSelectedTab("Overview")}
                style={{
                  color: selectedTab == "Overview" ? "#41414e" : "#969ba1",
                }}
              >
                Overview
              </button>

              <button
                className={`${
                  selectedTab === "Performance summary"
                    ? "bg-white "
                    : "bg-transparent "
                } fw-bold me-1 px-2 py-1 rounded border-0`}
                onClick={(e) => setSelectedTab("Performance summary")}
                data-bs-toggle="tab"
                style={{
                  color:
                    selectedTab == "Performance summary"
                      ? "#41414e"
                      : "#969ba1",
                }}
              >
                Performance summary
              </button>

              <button
                className={`${
                  selectedTab === "List of Trades"
                    ? "bg-white "
                    : "bg-transparent "
                } fw-bold me-1 px-2 py-1 rounded border-0`}
                onClick={(e) => setSelectedTab("List of Trades")}
                data-bs-toggle="tab"
                style={{
                  color:
                    selectedTab == "List of Trades" ? "#41414e" : "#969ba1",
                }}
              >
                List of Trades
              </button>

              <button
                className={`${
                  selectedTab === "Properties" ? "bg-white" : "bg-transparent"
                } fw-bold me-1 px-2 py-1 rounded border-0`}
                onClick={(e) => setSelectedTab("Properties")}
                data-bs-toggle="tab"
                style={{
                  color: selectedTab == "Properties" ? "#41414e" : "#969ba1",
                }}
              >
                Properties
              </button>
            </div>

            {/* <div className="mt-3 mt-md-0">
              <select
                name="pets"
                id="pet-select"
                className="dropdown-container"
              >
                <option value="">Select</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
              </select>
            </div> */}

            <DropdownButton
              id="dropdown-basic-button"
              title={"Select date range"}
              bsPrefix="px-4 py-2 border-0 rounded-2"
              style={{ backgroundColor: "white" }}
              onToggle={() => {
                setDropDown(!dropDown);
              }}
              show={dropDown}
            >
              <Dropdown>
                <DateRangeFilter setDropDown={setDropDown} />
              </Dropdown>
            </DropdownButton>
          </div>

          <div className="overflow-x-scroll">
            <table style={{ width: "2000px" }}>
              <tr>
                <th className="table-header ps-3">
                  Trade # V{" "}
                  <span className="cursor-pointer">
                    {sortColumn === "id" && sortingId ? (
                      <i
                        className="bi bi-arrow-down"
                        onClick={() => handleSort("id")}
                      ></i>
                    ) : (
                      <i
                        className="bi bi-arrow-up"
                        onClick={() => handleSort("id")}
                      ></i>
                    )}
                  </span>
                </th>
                <th className="table-header">
                  Display Name
                  <span className="cursor-pointer">
                    {sortColumn === "array_data.date_time.date_time1" &&
                    sortingId ? (
                      <i
                        className="bi bi-arrow-down"
                        onClick={() =>
                          handleSort("array_data.date_time.date_time1")
                        }
                      ></i>
                    ) : (
                      <i
                        className="bi bi-arrow-up"
                        onClick={() =>
                          handleSort("array_data.date_time.date_time1")
                        }
                      ></i>
                    )}
                  </span>
                </th>
                <th
                  className="table-header"
                  onClick={() => handleSort("array_data.signal.type1")}
                >
                  Strategy Name
                </th>
                <th
                  className="table-header"
                  onClick={() => handleSort("array_data.signal.type1")}
                >
                  Time Frame
                </th>
                <th
                  className="table-header"
                  onClick={() => handleSort("array_data.signal.type1")}
                >
                  Buy/Sell
                </th>
                <th
                  className="table-header"
                  onClick={() => handleSort("array_data.signal.type1")}
                >
                  Exit Type
                </th>
                <th
                  className="table-header"
                  onClick={() => handleSort("array_data.signal.type1")}
                >
                  Signal <br></br> Time Entry
                </th>
                <th
                  className="table-header"
                  onClick={() => handleSort("array_data.signal.type1")}
                >
                  Target <br></br> Stop loss
                </th>
                <th
                  className="table-header"
                  onClick={() => handleSort("array_data.signal.type1")}
                >
                  Entry Time <br></br> Entry price
                </th>
                <th
                  className="table-header"
                  onClick={() => handleSort("array_data.signal.type1")}
                >
                  Exit Time <br></br> Exit price
                </th>

                <th
                  className="table-header"
                  onClick={() => handleSort("array_data.signal.type1")}
                >
                  P&L <br></br> Absolute
                </th>
                <th
                  className="table-header"
                  // onClick={() => handleSort("cum_profit")}
                >
                  P&L %
                  <span className="cursor-pointer">
                    {sortColumn === "cum_profit" && sortingId ? (
                      <i
                        className="bi bi-arrow-down"
                        onClick={() => handleSort("cum_profit")}
                      ></i>
                    ) : (
                      <i
                        className="bi bi-arrow-up"
                        onClick={() => handleSort("cum_profit")}
                      ></i>
                    )}
                  </span>
                </th>
                {/* <th
                  className="table-header"
                  onClick={() => handleSort("array_data.date_time.date_time1")}
                >
                  Date/Time
                </th> */}
                {/* <th
                  className="table-header"
                  onClick={() => handleSort("array_data.price.price1")}
                >
                  Price
                </th> */}
                {/* <th
                  className="table-header"
                  onClick={() => handleSort("contract")}
                >
                  Contracts
                </th> */}
                {/* <th
                  className="table-header"
                  style={{ textAlign: "right" }}
                  onClick={() => handleSort("cum_profit")}
                >
                  Cum. Profit
                </th> */}
              </tr>

              {paginatedData?.map((val, i) => (
                <>
                  <tr
                    style={{
                      borderBottom: "1px solid #F4F5F5",
                    }}
                    className="table-data"
                  >
                    <td
                      rowSpan={2}
                      style={{ borderBottom: "1px solid #F4F5F5" }}
                      className="ps-3"
                    >
                      {i + 1}
                    </td>
                    <td
                      rowSpan={2}
                      style={{ borderBottom: "1px solid #F4F5F5" }}
                    >
                      {val.displayname}
                    </td>
                    <td
                      rowSpan={2}
                      style={{ borderBottom: "1px solid #F4F5F5" }}
                    >
                      {val.strategyname}
                    </td>
                    <td
                      rowSpan={2}
                      style={{ borderBottom: "1px solid #F4F5F5" }}
                    >
                      {val.timeframe}
                    </td>
                    <td
                      rowSpan={2}
                      style={{ borderBottom: "1px solid #F4F5F5" }}
                    >
                      {val.buysell}
                    </td>
                    <td
                      rowSpan={2}
                      style={{ borderBottom: "1px solid #F4F5F5" }}
                    >
                      {val.exittype}
                    </td>
                    <td className="py-4">
                      {/* {val.array_data.entry_signal_time.type1} */}
                      {val.type_array.EST.type2}
                    </td>
                    <td className="py-4">
                      {val.type_array.TS.type1}
                      {/* {val.array_data.target_stop_loss.type1} */}
                    </td>
                    <td className="py-4">
                      {val.type_array.ENTRY_TP.type1}{" "}
                      {/* {val.array_data.entry_time_price.type1} */}
                    </td>
                    <td className="py-4">
                      {/* {val.array_data.exit_time_price.type1} */}
                      {val.type_array.EXIT_TP.type1} {/* EXIT_TP */}
                    </td>
                    {/* <td className="py-4">{val.array_data.pl_absolute.type1}</td>
                    <td className="py-4">{val.array_data.pl_percent.type1}</td> */}
                    {/* <td
                      rowSpan={2}
                      style={{
                        borderBottom: "1px solid #F4F5F5",
                        color: "#f82b2b",
                        textAlign: "right",
                      }}
                    >
                      {val?.cum_profit?.split(" ")?.map((item) => {
                        return <div>{item}</div>;
                      })}
                    </td> */}
                  </tr>

                  <tr
                    style={{
                      borderBottom: "1px solid #F4F5F5",
                    }}
                    className="table-data"
                  >
                    <td className="py-4">
                      {/* EST */}
                      {val.type_array.EST.type2}
                      {/* {val.array_data.entry_signal_time.type2} */}
                    </td>
                    <td className="py-4">
                      {val.type_array.TS.type2}
                      {/* {val.array_data.target_stop_loss.type2} */}
                    </td>
                    <td className="py-4">{val.type_array.ENTRY_TP.type2} </td>
                    <td className="py-4">
                      {/* {val.array_data.exit_time_price.type2} */}
                      {val.type_array.EXIT_TP.type2}{" "}
                    </td>
                    <td className="py-4">
                      {val?.pl_absolute}
                      {/* {val.array_data.pl_absolute.type2} */}
                    </td>
                    <td className="py-4">
                      {val?.pl_percentage}
                      {/* {val.array_data.pl_percent.type2} */}
                    </td>
                  </tr>
                </>
              ))}
            </table>
          </div>
        </div>
      </div>
      <TableWithPagination
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        totalPages={totalPages}
        setTotalPages={setTotalPages}
        paginatedData={paginatedData}
        setPaginatedData={setPaginatedData}
        data={sortedData}
        itemsPerPage={visibleData ? visibleData : 10}
        setVisibleData={setVisibleData}
        visibleData={visibleData}
      />
    </div>
  );
};
export default StrategyAnalyserTable;
// import React from 'react';
// interface TableProps {
//   data: Array<{ id: number; name: string; age: number; colspan: number; rowspan: number }>;
// }
// const data = [
//   { id: 1, name: 'John Doe', age: 25, colspan: 2, rowspan: 1 },
//   { id: 2, name: 'Jane Doe', age: 30, colspan: 1, rowspan: 2 },
//   // Add more data as needed
// ];
// const StrategyAnalyserTable: React.FC<TableProps> = ({  }) => {
//   return (
//     <table>
//       <thead>
//         <tr>
//           <th>ID</th>
//           <th>Name</th>
//           <th>Age</th>
//         </tr>
//       </thead>
//       <tbody>
//         {data?.map((item) => (
//           <tr key={item.id}>
//             <td rowSpan={item.rowspan}>{item.id}</td>
//             <td colSpan={item.colspan}>{item.name}</td>
//             <td>{item.age}</td>
//           </tr>
//         ))}
//       </tbody>
//     </table>
//   );
// };
// export default StrategyAnalyserTable;
