import React, { useEffect, useState } from "react";
import TableTabs from "./TableTabs";



interface IPositionTableProps {
  selectedTab: string;
}

const StrategyPositionsTable: React.FC<IPositionTableProps> = ({
  selectedTab,
}) => {
  const dummyData = [
    {
      id: 1,
      strategy_name: "straddle",
      array_data: [
        {
          instrument: "instrument1",
          signal: "signal1",
          contracts: "contracts1",
          avg_price: "avg_price1",
          ltp: "ltp1",
          mtm_profit: "mtm_profit1",
          date_time: "date_time1",
        },
        {
          instrument: "instrument2",
          signal: "signal2",
          contracts: "contracts2",
          avg_price: "avg_price2",
          ltp: "ltp1",
          mtm_profit: "mtm_profit2",
          date_time: "date_time2",
        },
      ],
      contract: 1,
      cum_profit: "-59.25 INR -0.28%",
    },
    {
      id: 2,
      strategy_name: "Scalping",
      array_data: [
        {
          instrument: "instrument1",
          signal: "signal1",
          contracts: "contracts1",
          avg_price: "avg_price1",
          ltp: "ltp1",
          mtm_profit: "mtm_profit1",
          date_time: "date_time1",
        },
        {
          instrument: "instrument2",
          signal: "signal2",
          contracts: "contracts2",
          avg_price: "avg_price2",
          ltp: "ltp1",
          mtm_profit: "mtm_profit2",
          date_time: "date_time2",
        },
        {
          instrument: "instrument3",
          signal: "signal3",
          contracts: "contracts3",
          avg_price: "avg_price3",
          ltp: "ltp3",
          mtm_profit: "mtm_profit3",
          date_time: "date_time3",
        },
      ],
      contract: 1,
      cum_profit: "-59.25 INR -0.28%",
    },
    {
      id: 3,
      strategy_name: "Strangle",
      array_data: [
        {
          instrument: "instrument1",
          signal: "signal1",
          contracts: "contracts1",
          avg_price: "avg_price1",
          ltp: "ltp1",
          mtm_profit: "mtm_profit1",
          date_time: "date_time1",
        },
        {
          instrument: "instrument2",
          signal: "signal2",
          contracts: "contracts2",
          avg_price: "avg_price2",
          ltp: "ltp1",
          mtm_profit: "mtm_profit2",
          date_time: "date_time2",
        },
        {
          instrument: "instrument3",
          signal: "signal3",
          contracts: "contracts3",
          avg_price: "avg_price3",
          ltp: "ltp3",
          mtm_profit: "mtm_profit3",
          date_time: "date_time3",
        },
        {
          instrument: "instrument4",
          signal: "signal4",
          contracts: "contracts4",
          avg_price: "avg_price4",
          ltp: "ltp4",
          mtm_profit: "mtm_profit4",
          date_time: "date_time4",
        },
      ],
      contract: 1,
      cum_profit: "-59.25 INR -0.28%",
    },
    {
      id: 4,
      strategy_name: "Strangle",
      array_data: [
        {
          instrument: "instrument1",
          signal: "signal1",
          contracts: "contracts1",
          avg_price: "avg_price1",
          ltp: "ltp1",
          mtm_profit: "mtm_profit1",
          date_time: "date_time1",
        },
        // {
        //   instrument: "instrument2",
        //   signal: "signal2",
        //   contracts: "contracts2",
        //   avg_price: "avg_price2",
        //   ltp: "ltp1",
        //   mtm_profit: "mtm_profit2",
        //   date_time: "date_time2",
        // },
        // {
        //   instrument: "instrument3",
        //   signal: "signal3",
        //   contracts: "contracts3",
        //   avg_price: "avg_price3",
        //   ltp: "ltp3",
        //   mtm_profit: "mtm_profit3",
        //   date_time: "date_time3",
        // },
        // {
        //   instrument: "instrument4",
        //   signal: "signal4",
        //   contracts: "contracts4",
        //   avg_price: "avg_price4",
        //   ltp: "ltp4",
        //   mtm_profit: "mtm_profit4",
        //   date_time: "date_time4",
        // },
      ],
      contract: 1,
      cum_profit: "-59.25 INR -0.28%",
    },
  ];
  const [selectedTabId, setSelectedTabId] = useState<string>("");
  useEffect(() => {
    setSelectedTabId(tabs?.[0]?.id || "");
  }, [selectedTab]);

  const tabs =
    selectedTab === "Analysis"
      ? TableTabs.tabsForAnalysisScreen
      : TableTabs.tabsForPositionsScreen;
  const yourData = dummyData; /* Your data array */
  const itemsPerPage = 5; // Initial items per page
  const calculatePossibleOptions = (totalItems: number) => {
    const options: number[] = [];
    for (let i = 5; i <= totalItems; i += 5) {
      options.push(i);
    }
    return options;
  };

  return (
    <div className="new-table-card">
      <div>
        <div className="child-strategy-analyzer-table">
          <div className="d-flex">
            <div
              className="d-flex align-items-center mb-5"
              style={{
                backgroundColor: "#f0f0f0",
                width: "fit-content",
                padding: "4px",
                borderRadius: "4px",
              }}
            >
              {tabs?.map((data) => (
                <button
                  className={`${
                    selectedTabId == data?.id ? "bg-white " : "bg-transparent "
                  } fw-bold me-1 px-2 py-1 rounded border-0`}
                  onClick={(e) => setSelectedTabId(data?.id)}
                  data-bs-toggle="tab"
                  style={{
                    color: selectedTabId == data?.id ? "#41414e" : "#969ba1",
                  }}
                >
                  {data?.name}
                </button>
              ))}
            </div>
          </div>

          <div className="strategy-position-table-wrapper">
            <table className="strategy-position-table">
              <thead>
                <tr>
                  <th className="table-header ps-3">Strategy Name</th>
                  <th className="table-header">Instrument</th>
                  <th className="table-header">Signal</th>
                  <th className="table-header">Contracts</th>
                  <th className="table-header">Avg Price</th>
                  <th className="table-header">LTP</th>
                  <th className="table-header">MTM Profit</th>
                  <th className="table-header">Date/Time</th>
                </tr>
              </thead>

              <tbody>
                {dummyData?.map((val, i) => (
                  <>
                    <tr
                      className="table-data"
                      style={{ borderBottom: "1px solid #F4F5F5" }}
                    >
                      <td
                        rowSpan={val.array_data.length}
                        className="ps-3 fs-18"
                        style={{
                          borderBottom:
                            i == dummyData.length - 1
                              ? ""
                              : "1px solid #F4F5F5",
                        }}
                      >
                        {val.strategy_name}
                      </td>
                      <td className="py-4">{val.array_data[0].instrument}</td>
                      <td className="py-4">{val.array_data[0].signal}</td>
                      <td className="py-4">{val.array_data[0].contracts}</td>
                      <td className="py-4">{val.array_data[0].avg_price}</td>
                      <td className="py-4">{val.array_data[0].ltp}</td>
                      <td className="py-4">{val.array_data[0].mtm_profit}</td>
                      <td className="py-4">{val.array_data[0].date_time}</td>
                    </tr>

                    {val.array_data.length > 1 &&
                      val?.array_data?.map((data, index) => (
                        <>
                          {index !== 0 && (
                            <tr
                              className="table-data"
                              style={{
                                borderBottom:
                                  index == val.array_data.length - 1 &&
                                  i == dummyData.length - 1
                                    ? ""
                                    : "1px solid #F4F5F5",
                              }}
                            >
                              <td className="py-4">{data.instrument}</td>
                              <td className="py-4">{data.signal}</td>
                              <td className="py-4">{data.contracts}</td>
                              <td className="py-4">{data.avg_price}</td>
                              <td className="py-4">{data.ltp}</td>
                              <td className="py-4">{data.mtm_profit}</td>
                              <td className="py-4">{data.date_time}</td>
                            </tr>
                          )}
                        </>
                      ))}
                  </>
                ))}
              </tbody>

              {/* <Pagination data={yourData} itemsPerPage={itemsPerPage} /> */}
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};
export default StrategyPositionsTable;
