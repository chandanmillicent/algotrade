import React, { useState } from "react";
import PropTypes from "prop-types";

import "react-date-range/dist/styles.css"; // main css file
import "react-date-range/dist/theme/default.css"; // theme css file
import { DateRangePicker } from "react-date-range";
import { addDays, subDays, startOfMonth, endOfDay, setDate } from "date-fns";
import { Button } from "react-bootstrap";
import moment from "moment";

interface IpropsData {
  // onChange: (selection: object) => void;
  setSelectedDate?: (selectedDate: object) => void;
  setDropDown: (dropDown: boolean) => void;
}

const DateRangeFilter: React.FC<IpropsData> = ({
  setSelectedDate = () => {},
  setDropDown = () => {},
}) => {
  const [state, setState] = useState([
    {
      startDate: startOfMonth(new Date()),
      endDate: endOfDay(new Date()),
      key: "selection",
    },
  ]);

  const handleOnChange = (ranges: any) => {
    console.log("r", ranges);
    const { selection } = ranges;
    // onChange(selection);
    setState([selection]);
  };

  const ResetFilter = () => {
    setState([
      {
        startDate: startOfMonth(new Date()),
        endDate: endOfDay(new Date()),
        key: "selection",
      },
    ]);
  };

  const handleApplyDateFilter = () => {
    setSelectedDate(state);
    console.log("state", state);
    setDropDown(false);
    // setState({ startDate: startDate, endDate: endDate });
    // setanchorCalendar(null);
  };

  return (
    <>
      <DateRangePicker
        onChange={handleOnChange}
        // showSelectionPreview={true}
        moveRangeOnFirstSelection={false}
        months={2}
        ranges={state}
        maxDate={new Date()}
        direction="horizontal"
        className="date-range-container"
      />
      <div className="bg-light p-4 d-flex justify-content-end">
        <Button
          className="btn btn-secondary"
          type="button"
          onClick={() => ResetFilter()}
        >
          Reset
        </Button>
        <Button
          className="btn btn-primary ms-5"
          type="button"
          onClick={() => handleApplyDateFilter()}
        >
          Apply
        </Button>
      </div>
    </>
  );
};

// DateRangeFilter.propTypes = {
//   onChange: PropTypes.func,
// };

export default DateRangeFilter;
