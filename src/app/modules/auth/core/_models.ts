export interface AuthModel {
  api_token: string;
  refreshToken?: string;
}

export interface StrategySingleDataModel {
  win_loss_ratio: number;
  net_pl: number;
  total_profit: number;
  total_loss: number;
  win_ratio: number;
  loss_ratio: number;
  average_win: number;
  average_loss: number;
  no_win_trade: number;
  no_loss_trade: number;
  max_win: number;
  max_loss: number;
}

export interface IStretagyHeader {
  id: number;
  strategy_name: string;
  display_name: string;
}
export interface IStretagyBarChart {
  amount: number;
  month: string;
  type: string;
  color: string;
}

export interface UserAddressModel {
  addressLine: string;
  city: string;
  state: string;
  postCode: string;
}

export interface ITypeKeyVAlue {
  type1: number;
  type2: string;
  type1_name: string;
  type2_name: string;
}
export interface ITypeArray {
  EXIT_TP: ITypeKeyVAlue;
  ENTRY_TP: ITypeKeyVAlue;
  EST: ITypeKeyVAlue;
  TS: ITypeKeyVAlue;
  ETEP: ITypeKeyVAlue;
  ETET: ITypeKeyVAlue;
}

export interface IStrategyData {
  key_name: string[] | unknown;
  name: string[] | unknown;
  type: string[] | unknown;
  value: number[] | unknown;
}
export interface ITableData {
  displayname: string;
  strategyname: string;
  timeframe: string;
  buysell: string;
  exittype: string;
  pl_absolute: string;
  pl_percentage: string;
  type_array: ITypeArray;
  cum_profit?: any;
}
interface Itabs {
  id: number;
  strategy_name: string;
  display_name: string;
}
interface tabsForSecondStep {
  id: number;
  cardName: string;
}

export interface UserCommunicationModel {
  email: boolean;
  sms: boolean;
  phone: boolean;
}

export interface UserEmailSettingsModel {
  emailNotification?: boolean;
  sendCopyToPersonalEmail?: boolean;
  activityRelatesEmail?: {
    youHaveNewNotifications?: boolean;
    youAreSentADirectMessage?: boolean;
    someoneAddsYouAsAsAConnection?: boolean;
    uponNewOrder?: boolean;
    newMembershipApproval?: boolean;
    memberRegistration?: boolean;
  };
  updatesFromKeenthemes?: {
    newsAboutKeenthemesProductsAndFeatureUpdates?: boolean;
    tipsOnGettingMoreOutOfKeen?: boolean;
    thingsYouMissedSindeYouLastLoggedIntoKeen?: boolean;
    newsAboutStartOnPartnerProductsAndOtherServices?: boolean;
    tipsOnStartBusinessProducts?: boolean;
  };
}

export interface UserSocialNetworksModel {
  linkedIn: string;
  facebook: string;
  twitter: string;
  instagram: string;
}

export interface UserModel {
  id: number;
  username: string;
  password: string | undefined;
  email: string;
  first_name: string;
  last_name: string;
  fullname?: string;
  occupation?: string;
  companyName?: string;
  phone?: string;
  roles?: Array<number>;
  pic?: string;
  language?: "en" | "de" | "es" | "fr" | "ja" | "zh" | "ru";
  timeZone?: string;
  website?: "https://keenthemes.com";
  emailSettings?: UserEmailSettingsModel;
  auth?: AuthModel;
  communication?: UserCommunicationModel;
  address?: UserAddressModel;
  socialNetworks?: UserSocialNetworksModel;
}
