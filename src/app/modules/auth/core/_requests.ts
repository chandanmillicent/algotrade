import axios from "axios";
import {
  AuthModel,
  IStretagyBarChart,
  IStretagyHeader,
  ITableData,
  StrategySingleDataModel,
  UserModel,
  IStrategyData,
} from "./_models";
const API_URL = import.meta.env.VITE_APP_API_URL;

export const GET_USER_BY_ACCESSTOKEN_URL = `${API_URL}/verify_token`;
export const LOGIN_URL = `${API_URL}/login`;
export const LOGOUT_URL = `${API_URL}/logout`;
export const REGISTER_URL = `${API_URL}/register`;
export const GET_STRATEGY_HEADER_URL = `${API_URL}/strategy`;
export const GET_STRATEGY_BAR_CHART_URL = `${API_URL}/barchart`;
export const GET_STRATEGY_CARD_URL = `${API_URL}/strategy/card`;
export const GET_STRATEGY_TABLE_URL = `${API_URL}/strategy/overview`;
export const REQUEST_PASSWORD_URL = `${API_URL}/forgot_password`;

// export function _headers(): Record<string, string> {
//   const header: Record<string, string> = {}

//   if (localStorage.getItem("kt-auth-token-v")) {
//     const auth = localStorage.getItem("kt-auth-token-v")
//     header["Authorization"] = "Bearer " + auth
//   }
//   return header
// }
// Server should return AuthModel
export function login(email: string, password: string) {
  return axios.post<AuthModel>(
    LOGIN_URL,
    {
      email,
      password,
    },
    {
      headers: {
        "Content-type": "application/x-www-form-urlencoded",
      },
    }
  );
}

export function logoutUser() {
  return axios.post<AuthModel>(LOGOUT_URL, {});
}
export async function getStrategyCardData(
  buyselltype: string,
  headerType: string
) {
  const url =
    GET_STRATEGY_CARD_URL +
    "/header/" +
    headerType +
    "/buyselltype/" +
    buyselltype;
  const data = await axios.get<IStrategyData[]>(url);
  return data.data;
}

export async function getStrategyBarChart(
  header: string,
  buyselltype: string,
  datetype: string
) {
  const url =
    GET_STRATEGY_BAR_CHART_URL +
    "/header/" +
    header +
    "/buyselltype/" +
    { buyselltype } +
    "/datetype/" +
    datetype;
  const data = await axios.get<IStretagyBarChart[]>(url);
  return data.data;
}
export async function getStrategyHeader() {
  const url = GET_STRATEGY_HEADER_URL;
  const data = await axios.get<IStretagyHeader[]>(url, {
    headers: {
      "Content-type": "application/x-www-form-urlencoded",
    },
  });
  return data.data;
}
export async function getStrategyOverview(
  buyselltype: string,
  headerType: string
) {
  const url =
    GET_STRATEGY_TABLE_URL +
    "/header/" +
    headerType +
    "/buyselltype/" +
    buyselltype;
  const response = await axios.get<ITableData[]>(url);
  return response.data;
}

// Server should return AuthModel
export function register(
  email: string,
  firstname: string,
  lastname: string,
  password: string,
  password_confirmation: string
) {
  return axios.post(REGISTER_URL, {
    email,
    first_name: firstname,
    last_name: lastname,
    password,
    password_confirmation,
  });
}

// Server should return object => { result: boolean } (Is Email in DB)
export function requestPassword(email: string) {
  return axios.post<{ result: boolean }>(REQUEST_PASSWORD_URL, {
    email,
  });
}

export function getUserByToken(token: string) {
  return axios.post<UserModel>(
    GET_USER_BY_ACCESSTOKEN_URL,
    {
      api_token: token,
    },
    {
      headers: {
        "Content-type": "application/x-www-form-urlencoded",
      },
    }
  );
}
