import { FC, useEffect, useState } from "react";
import { useIntl } from "react-intl";
import { PageTitle } from "../../../_metronic/layout/core";
import { Content } from "../../../_metronic/layout/components/content";
import PIeChart from "../../../_metronic/partials/widgets/_new/cards/PIeChart";
import SmallSizeCard from "../../../_metronic/partials/widgets/_new/cards/SmallSizeCard";
import { Col, Row } from "react-bootstrap";
import RadioButton from "../../../_metronic/layout/components/radioButton/RadioButton";
import DashBoardTab from "../../../_metronic/layout/components/redio-button/DashBoardTab";
import StrategyAnalyserTable from "../../../_metronic/layout/components/table/StrategyAnalyserTable";
import {
  CardsWidget17,
  ChartsWidget3,
} from "../../../_metronic/partials/widgets";
import { useMyContext } from "../../modules/auth/core/MyContext";
import BarChart from "../../../_metronic/partials/widgets/charts/BarChart";
import { getStrategyHeader } from "../../modules/auth/core/_requests";
import StrategyPositionsTable from "../../../_metronic/layout/components/table/StrategyPositionsTable";
import { IStretagyHeader } from "../../modules/auth";

type IButtonAction = string;

const DashboardPage: React.FC = () => {
  const [context, setContext] = useState<IButtonAction>("ALL");
  const [headerType, setHeaderType] = useState<string>("ALL");
  console.log("🚀 ~ headerType:", headerType);
  const [headerData, setHeaderData] = useState<IStretagyHeader[] | null>([]);
  console.log("🚀 ~ headerData:", headerData);
  const { secondStep } = useMyContext();
  const [selectedTab, setSelectedTab] = useState<string>("Analysis");

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await getStrategyHeader();
        setHeaderData(response);
      } catch (err) {
        console.log(err);
      }
    };

    fetchData();
  }, []);

  // console.log(headerData, "headerData")

  const handleClick = (tab: string) => {
    setSelectedTab(tab);
  };

  interface dummyBarData {
    id: number;
    month: string;
    key: string;
    amount: string;
    color: string;
  }
  const dummyBarData: dummyBarData[] = [
    {
      id: 1,
      month: "jan",
      key: "loss",
      amount: "40000",
      color: "red",
    },
    {
      id: 2,
      month: "feb",
      key: "profit",
      amount: "400",
      color: "green",
    },
    {
      id: 3,
      month: "march",
      key: "loss",
      amount: "70000",
      color: "red",
    },
    {
      id: 4,
      month: "april",
      key: "loss",
      amount: "20000",
      color: "red",
    },

    // Add more users as needed
  ];

  return (
    <>
      {/* <ToolbarWrapper /> */}
      <div className="pb-10">
        <Content>
          <div>
            <div className="d-flex gap-5">
              <div
                className={`cursor-pointer ${
                  selectedTab === "Analysis"
                    ? "green border-buttom fw-600 fs-18"
                    : "gray fs-18"
                }`}
                onClick={() => handleClick("Analysis")}
                // style={`${selectedTab === "Analysis" ? {borderBottom: "1px solid green" ,paddingBottom}: "3px" : ""}`}
              >
                {/* <span> */}
                Analysis
                {/* </span> */}
              </div>
              <div
                className={`cursor-pointer ${
                  selectedTab === "Positions"
                    ? "green border-buttom fw-600 fs-18"
                    : "gray fs-18"
                }`}
                onClick={() => handleClick("Positions")}
              >
                Positions
              </div>
            </div>
          </div>

          <div className="mt-5">
            <DashBoardTab headerData={headerData} />
          </div>
          {/* <div className="pb-5">
            <StrategyPositionsTable selectedTab={selectedTab} />
          </div> */}
          {secondStep != true ? (
            <>
              {" "}
              <div className="mb-5 mt-4 d-flex gap-7">
                <RadioButton
                  setContext={setContext}
                  selectedTab={selectedTab}
                  // setSelectedTab={setSelectedTab}
                />
              </div>
              {selectedTab === "Analysis" && (
                <Row>
                  {/* chart ====================================*/}

                  <Col xl={6}>
                    <div style={{ height: "430px" }}>
                      <BarChart
                        buyselltype={context}
                        headerType={headerType}
                      />
                    </div>
                  </Col>

                  <Col
                    sm={6}
                    xl={3}
                    className="mt-8 mt-xl-0"
                  >
                    <div style={{ height: "430px" }}>
                      <CardsWidget17 />
                    </div>
                  </Col>

                  <Col
                    sm={6}
                    xl={3}
                    className="mt-8 mt-xl-0"
                  >
                    {/* <CardsWidget17 className="h-md-100" /> */}
                    <div style={{ height: "430px" }}>
                      <PIeChart />
                    </div>
                  </Col>
                </Row>
              )}
              <div className="col-xxl-4">
                <SmallSizeCard
                  context={context}
                  headerType={headerType}
                />
              </div>
            </>
          ) : (
            <>
              <div className="mt-5">
                <ChartsWidget3 />
              </div>
            </>
          )}
          {selectedTab === "Analysis" ? (
            <div className="pb-5 mt-6">
              <StrategyAnalyserTable
                context={context}
                headerType={headerType}
              />
            </div>
          ) : (
            <div className="pb-5">
              <StrategyPositionsTable selectedTab={selectedTab} />
            </div>
          )}
        </Content>
      </div>
    </>
  );
};

{
  /* // const DashboardPage: FC = () => {
//   const chartData = [30, 40, 20, 10];
//   const chartLabels = ['Label 1', 'Label 2', 'Label 3', 'Label 4'];
//   <>
// <ToolbarWrapper />
// <Content>
//   {/* begin::Row */
}
{
  /* //   <div className="row g-5 g-xl-10 mb-5 mb-xl-10"> */
}
//     {/* begin::Col */}
//     {/* <div className="col-md-6 col-lg-6 col-xl-6 col-xxl-3 mb-md-5 mb-xl-10">
//       <CardsWidget20
//         className="h-md-50 mb-5 mb-xl-10"
//         description="Active Projects"
//         color="#F1416C"
//         img={toAbsoluteUrl("media/patterns/vector-1.png")}
//       />
//       <CardsWidget7
//         className="h-md-50 mb-5 mb-xl-10"
//         description="Professionals"
//         icon={false}
//         stats={357}
//         labelColor="dark"
//         textColor="gray-300"
//       />
//     </div> */}
//     {/* end::Col */}
//     {/* <div className="row g-5 g-xl-8">
//       <div className="col-xl-6">
//         <ChartsWidget6 className="card-xl-stretch mb-xl-8" />
//       </div>
//     </div> */}

//     {/* begin::Col */}
//     {/* chart ====================================*/}
{
  /* //     <div className="col-xxl-6">
//       <ChartsWidget6 className="h-md-100" />
//     </div>
//     <div className="col-md-6 col-lg-6 col-xl-6 col-xxl-3  ">
//     <DoughnutChart data={chartData} labels={chartLabels} />
//       {/* <CardsWidget17 className="h-md-100" /> */
}
//     </div> */}
//     {/* end::Col */}

//     {/* begin::Col */}

//     <div className="col-md-6 col-lg-6 col-xl-6 col-xxl-3 ">
//       <CardsWidget17 className="h-md-100" />
//       {/* <ListsWidget26 className="h-lg-50" /> */}
//     </div>
//     {/* end::Col */}
//   </div>
//   {/* end::Row */}

//   {/* begin::Row */}
//   <div className="row gx-5 gx-xl-10">
//     {/* begin::Col */}
//     <div className="col-xxl-6 mb-5 mb-xl-10">
//       {/* <app-new-charts-widget8 cssclassName="h-xl-100" chartHeight="275px" [chartHeightNumber]="275"></app-new-charts-widget8> */}
//     </div>
//     {/* end::Col */}

//     {/* begin::Col */}
//     <div className="col-xxl-6 mb-5 mb-xl-10">
//       {/* <app-cards-widget18 cssclassName="h-xl-100" image="./assetsmedia/stock/600x600/img-65.jpg"></app-cards-widget18> */}
//     </div>
//     {/* end::Col */}
//   </div>
//   {/* end::Row */}

//   {/* begin::Row */}
//   <div className="row gy-5 gx-xl-8">
//     <div className="col-xxl-4">
//       <ListsWidget3 className="card-xxl-stretch mb-xl-3" />
//     </div>
//     <div className="col-xl-8">
//       <TablesWidget10 className="card-xxl-stretch mb-5 mb-xl-8" />
//     </div>
//   </div>
//   {/* end::Row */}

//   {/* begin::Row */}
//   <div className="row gy-5 g-xl-8">
//     <div className="col-xl-4">
//       <ListsWidget2 className="card-xl-stretch mb-xl-8" />
//     </div>
//     <div className="col-xl-4">
//       <ListsWidget6 className="card-xl-stretch mb-xl-8" />
//     </div>
//     <div className="col-xl-4">
//       <ListsWidget4 className="card-xl-stretch mb-5 mb-xl-8" items={5} />
//       {/* partials/widgets/lists/_widget-4', 'class' => 'card-xl-stretch mb-5 mb-xl-8', 'items' => '5' */}
//     </div>
//   </div>
//   {/* end::Row */}

//   <div className="row g-5 gx-xxl-8">
//     <div className="col-xxl-4">
//       <MixedWidget8
//         className="card-xxl-stretch mb-xl-3"
//         chartColor="success"
//         chartHeight="150px"
//       />
//     </div>
//     <div className="col-xxl-8">
//       <TablesWidget5 className="card-xxl-stretch mb-5 mb-xxl-8" />
//     </div>
//   </div>
// </Content>
//   </>
// }; */}

const DashboardWrapper: FC = () => {
  const intl = useIntl();
  return (
    <>
      <PageTitle breadcrumbs={[]}>
        {intl.formatMessage({ id: "MENU.DASHBOARD" })}
      </PageTitle>
      <DashboardPage />
    </>
  );
};

export { DashboardWrapper };
